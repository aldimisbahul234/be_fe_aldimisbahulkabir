<?php
 
namespace App\Controllers;
 
use App\Controllers\BaseController;
use CodeIgniter\API\ResponseTrait;
use App\Models\UserModel;
use CodeIgniter\HTTP\RequestInterface;

class User extends BaseController
{
    use ResponseTrait;
     
    public function index()
    {
        $users = new UserModel;
        return $this->respond(['users' => $users->findAll()], 200);
    }
    public function detail($id)
    {
        $users = new UserModel;
        return $this->respond(['users' => $users->find($id)], 200);
    }
    public function create(){
        $userModel = new UserModel();
        $user['email'] = $this->request->getVar('email');
        $user['name'] = $this->request->getVar('name');
        $user['phone'] = $this->request->getVar('phone');
        $user['address'] = $this->request->getVar('address');
        $file = $this->request->getFile('image');
        if ($file != null) {
            $image = $this->request->getFile('image');
            $newName = $image->getRandomName();
            $image->move('./uploads', $newName);

            $user['image'] = $newName;
        }
        $userModel->save($user);
        return $this->respond(['users' => 'oke'], 200);
    }
    public function delete($id){
        $model = new UserModel();
        $model->where('id', $id)->delete();
        return $this->respond(['users' => 'oke'], 200);
    }
    public function update(){
        $userModel = new UserModel();
        $user = $userModel->find($this->request->getVar('id'));
        $user['email'] = $this->request->getVar('email');
        $user['name'] = $this->request->getVar('name');
        $user['phone'] = $this->request->getVar('phone');
        $user['address'] = $this->request->getVar('address');
        $file = $this->request->getFile('image');
        if ($file != null) {
            $image = $this->request->getFile('image');
            $newName = $image->getRandomName();
            $image->move('./uploads', $newName);

            $user['image'] = $newName;
        }
        $userModel->save($user);
         
            return $this->respond(['users' => 'oke'], 200);
       
        
    }
}