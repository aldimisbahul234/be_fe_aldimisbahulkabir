<?php namespace App\Database\Seeds;
  
class UserSeeder extends \CodeIgniter\Database\Seeder
{
    public function run()
    {
        $data = [
            [
                'email'  => 'user@gmail.com',
                'password'  =>  password_hash(12345678, PASSWORD_DEFAULT)
            ],
            [
                'email'  => 'admin@gmail.com',
                'password'  =>  password_hash(12345678, PASSWORD_DEFAULT)
            ]
        ];
        $this->db->table('users')->insertBatch($data);
    }
} 