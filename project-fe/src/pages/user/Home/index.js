import React, { useEffect, useState } from "react";
import Carousel, { CarouselItem } from "../../../components/Carousel";
import Navbar from "../../../components/Navbar";
import { userIdContext } from "../../../routes";

import DefaultPicture from '../../../assets/maternity.png';

import './index.css';
import { Link } from "react-router-dom";
import { getBanners } from "../../../services/Actions/BannerAction";
import { IMAGES_URL } from "../../../config";
import { getUserAction } from "../../../services/Actions/UserAction";

const HomePage = () => {
  const [banners, setBanners] = useState([]);
  const [user, setUser] = useState({});

  const id = React.useContext(userIdContext);

  const getProfile = (image) => {
    if (image) return `${IMAGES_URL}/${image}`
    else return DefaultPicture;
  }

  useEffect(() => {
    getBanners('home')
      .then(res => (setBanners(res)))
      .catch(e => (console.log(e.message)));
    
    getUserAction(id, true)
      .then(res => (setUser(res)))
      .catch(e => (console.log(e.message)));
  }, [id]);

  return (
    <div>
      <div className="navbar-body section-home">
        <Carousel>
          {
            banners.map(banner => (
              <CarouselItem key={banner.id}>
                <a href={banner.link} target='_blank' rel='noreferrer'>
                  <img src={`${IMAGES_URL}/${banner.image}`} alt="Banner"/>
                </a>
              </CarouselItem>
            ))
          }
        </Carousel>

        <img 
          className="profile-picture"
          src={ getProfile(user.image) }
          onError={({ target }) => {
            target.onError = null;
            target.src = DefaultPicture;
          }}
          alt="Profile"
        />

        <p>
          Halo, Ibu Canggih<br/>
          <b>{ user.name }</b>
        </p>

        <div className="section-point">
          <p>
            Poin Ibu sekarang<br/>
            <b>{user.point}</b>
          </p>

          <Link to='/reward/add'>
            <button className="add">Tambah Poin Reward</button>
          </Link>
          <Link to='/reward'>
            <button className="detail">Detail Poin Reward</button>
          </Link>
        </div>

      </div>

      <Navbar active="home"/>
    </div>
  )
}

export default HomePage;