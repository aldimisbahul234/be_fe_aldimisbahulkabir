import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import AdminMenu from "../../../components/AdminMenu";
import { createUserAction, getUserAction } from "../../../services/Actions/UserAction";
import { errorHandler } from "../../../services/Utils";

const AdminUserEditPage = () => {
  const [user, setUser] = useState();
  const [image, setimage] = useState(null);
  const [errorMessage, setErrorMessage] = useState();
  const { id } = useParams();
  const navigate = useNavigate();

  const handleFileChange = (event) => {
    const file = event.target.files[0];
    if (file && file.size > 1048576) { // 1MB = 1048576 bytes
      setErrorMessage('File Max Size')
      console.log('error')
    } else {
      setimage(file)
      console.log('oke')
    }
    
  };

  const editUser = (e) => {
    e.preventDefault();
    
    createUserAction({
      name: e.target.name.value,
      phone: e.target.phone.value,
      email: e.target.email.value,
      address: e.target.address.value,
      image:image
    })
      .then(() => (navigate(-1)))
      .catch(e => { errorHandler(e, setErrorMessage); })
  }

  useEffect(() => {
    getUserAction(id)
      .then(res => {
        setUser(res);
      })
      .catch(e => console.log(e.message));
  }, [id]);

  return(
    <div className="admin">
      <AdminMenu />
      <div className="body-admin">
        <form onSubmit={editUser}>
          <div className="card">
            <div className="card-header">
              Edit User
            </div>
            <div className="card-body">
              <div className="row">
                <div className="col-lg-6 mb-4">
                  <input type='text' className="form-control" name='name' placeholder="Nama" defaultValue={user && user.name}/>
                </div>
                <div className="col-lg-6 mb-4">
                  <input type='email' className="form-control" name='email' placeholder="Email" defaultValue={user && user.email}/>
                </div>
                <div className="col-lg-6 mb-4">
                  <input type='tel' name='phone' className="form-control" placeholder="No. Handphone" defaultValue={user && user.phone}/>
                </div>
                <div className="col-lg-6 mb-4">
                <input name="image" type="file" className="form-control" onChange={handleFileChange} />
                </div>
                <div className="col-lg-12 mb-4">
                  <textarea type='text' className="form-control" name='address' placeholder="Alamat" defaultValue={user && user.address}/>
                </div>
                <div className="col-lg-12 mb-4">
                  <button type="submit" className="btn btn-success">Create</button>
                </div>
              </div>
            </div>
          </div>
          { errorMessage && (<div className="error">{errorMessage}</div>) }
        </form>
      </div>
    </div>
  )
}

export default AdminUserEditPage;