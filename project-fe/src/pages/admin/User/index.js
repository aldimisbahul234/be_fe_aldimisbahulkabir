import { useEffect, useState } from "react";
import AdminMenu from "../../../components/AdminMenu";
import { deleteUserAction, exportUsersAction, getUsersAction, resetPasswordAction } from "../../../services/Actions/UserAction";

import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';
import { deleteUserFormAction } from "../../../services/Actions/EventAction";

import './index.css';
import Pagination from "../../../components/Pagination";
import { Link } from "react-router-dom";
import { deleteAllUserForm } from "../../../services/Api/Event";
import { IMAGES_URL } from "../../../config";
import 'bootstrap/dist/css/bootstrap.min.css';
import { FaBeer, FaUserAlt, FaRocketchat, FaClipboardList, FaHotjar, FaPhotoVideo, FaPowerOff, FaUserEdit, FaUserCog, FaTrash, FaSearch } from 'react-icons/fa';
const UserPage = () => {
  const [users, setUsers] = useState([]);
  const [total, setTotal] = useState(0);
  const [searchKey, setSearchKey] = useState();
  const [currentPage, setCurrentPage] = useState();
  const [totalPage, setTotalPage] = useState();

  const searchName = (e) => {
    e.preventDefault();
    const query = e.target.name.value;
    setSearchKey(query);
  }

  const deleteUser = (id) => {
    confirmAlert({
      title: 'Delete User',
      message: 'Are you sure want to do this?',
      buttons: [
        {
          label: 'Yes',
          onClick: () => {
            deleteUserAction(id)
              .then(() => window.location.reload() )
              .catch(e => console.log(e.message));
          }
        },
        {
          label: 'No'
        }
      ]
    });
  }

  const resetAllPoint = () => {
    confirmAlert({
      title: 'Reset Point ALL USERS',
      message: 'Are you sure want to do this?',
      buttons: [
        {
          label: 'Yes',
          onClick: () => {
            deleteAllUserForm()
              .then(() => {
                alert(`Reset Point Success`);
                window.location.reload();
              })
              .catch(e => console.log(e.message));
          }
        },
        {
          label: 'No'
        }
      ]
    });
  }

  const resetPoint = (id) => {
    confirmAlert({
      title: 'Reset Point',
      message: 'Are you sure want to do this?',
      buttons: [
        {
          label: 'Yes',
          onClick: () => {
            deleteUserFormAction(id)
              .then(() => {
                alert(`Reset Point Success`);
                window.location.reload();
              })
              .catch(e => console.log(e.message));
          }
        },
        {
          label: 'No'
        }
      ]
    });
  }

  const resetPassword = (id) => {
    confirmAlert({
      title: 'Reset password',
      message: 'Are you sure want to do this?',
      buttons: [
        {
          label: 'Yes',
          onClick: () => {
            resetPasswordAction(id)
              .then(res => {
                alert(`New Password: ${res.password}`) 
              })
              .catch(e => console.log(e.message));
          }
        },
        {
          label: 'No'
        }
      ]
    });
  }
  
   const prevPage = () => {
     if (currentPage && currentPage > 1) {
       setCurrentPage(currentPage - 1);
     }
   }

   const nextPage = () => {
     if (currentPage && totalPage && currentPage < totalPage) {
       setCurrentPage(currentPage + 1);
     }
   }

   const exportToExcel = () => {
     exportUsersAction()
      .then((response) => {
        const url = window.URL.createObjectURL(new Blob([response.data]));
        const link = document.createElement('a');
        link.href = url;
        link.setAttribute('download', 'users.xlsx');
        document.body.appendChild(link);
        link.click();
      })
      .catch(e => console.log(e.message));
   }

  useEffect(() => {
    const PAGE_SIZE = 50;
    if (!currentPage) setCurrentPage(1);
    else {
      getUsersAction({
          searchKey: searchKey, 
          page: currentPage,
          pageSize: PAGE_SIZE
        })
        .then(res => {
          setUsers(res.users);
          // setTotal(res.total_items);
          // setTotalPage(res.last_page);
        })
        .catch(e => (console.log(e.message)));
    }
  }, [searchKey, currentPage]);

  return (
    <div className="admin">
      <AdminMenu />
        <div className="body-admin">
          <div className="card">
            <div className="card-header">User Management</div>
            <Link style={{textAlign: 'right'}}  to={`/user/create`}><button style={{marginTop: '10px',marginRight: '10px'}} className="btn btn-primary btn-sm"><FaUserEdit /> Create user</button></Link>
            <div className="card-body">
                <table className="table table-striped">
                  <thead style={{backgroundColor:'#85296B', color: 'white'}}>
                    <tr>
                      <th>Id</th>
                      <th>Nama</th>
                      <th>Email</th>
                      <th>No. Handphone</th>
                      <th>Image</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    {
                      users.map(user => (
                        <tr key={user.id}>
                          <th>{user.id}</th>
                          <th>{user.name}</th>
                          <th>{user.email}</th>
                          <th>{user.phone}</th>
                          <th><img src={`${IMAGES_URL}/${user.image}`} alt="Banner"/></th>
                          <th>
                            <Link to={`/user/${user.id}`}><button className="btn btn-primary btn-sm"><FaUserEdit /> Edit</button></Link>
                            <button className="btn btn-danger btn-sm" onClick={() => deleteUser(user.id)}><FaTrash /> Delete</button>
                          </th>
                        </tr>
                      ))
                    }
                </tbody>
              </table>
              <Pagination current={currentPage} total={totalPage} prev={prevPage} next={nextPage}/>
              <br/>        
            </div>
          </div>
        </div>
    </div>
  )
}

export default UserPage;