import './index.css';

import Logo from '../../../assets/logo.png';
import { useState } from 'react';
import { Link } from 'react-router-dom';
import Footer from '../../../components/Footer';
import { loginAction } from '../../../services/Actions/UserAction';
import { errorHandler } from '../../../services/Utils';
import 'bootstrap/dist/css/bootstrap.min.css';
const LoginPage = ()  => {
  const [errorMessage, setErrorMessage] = useState();

  const handleLogin = async (e) => {
    e.preventDefault();

    loginAction({
      email: e.target.email.value,
      password: e.target.password.value
    })
      .then(() => { 
        window.location.reload() 
      })
      .catch(e => { errorHandler(e, setErrorMessage); })
  };

  return (
      <div>
        <div className="container-fluid d-flex justify-content-center" style={{marginTop: '200px'}}>
          {/* <Link to="/"><img src={ Logo } alt="Logo" /></Link> */}
            <div className='card'>
              <div className='card-body'>
                  <form onSubmit={ handleLogin }>
                    <div className='row'>
                      <div className='col-lg-12 mb-3'>
                        <div className='form-group'>
                          <input type="tel" className='form-control' id='email' name="email" placeholder="Email"/>
                        </div>
                      </div>
                      <div className='col-lg-12 mb-3'>
                        <div className='form-group'>
                          <input type="password" className='form-control' id='password' name="password" placeholder="Kata Sandi" />
                        </div>
                        { errorMessage && (<div className="error">{errorMessage}</div>) }
                      </div>
                      <div className='col-lg-12 mb-3'>
                        <div className='form-group'>
                            <button className='btn btn-success' type='submit'> Masuk</button>
                        </div>
                      </div>
                    </div>
                  </form>
              </div>
            </div>
            <div className='navigation'>
              Belum punya Akun Ibu-Ibu Canggih?
              <Link to="/register"><b> Daftar disini</b></Link>
            </div>
        </div>
      </div>
  )
}

export default LoginPage;