import { createEvent, createEventType, createForm, deleteAllUserForm, deleteEvent, deleteEventType, deleteUserForm, editEvent, editEventType, editUserForms, getEvent, getEventCalendar, getEvents, getEventType, getEventTypes, getForms, getPendingForms } from "../Api/Event";
import { assertNotNullOrBlank } from "../Utils";

export async function getCalendarAction(data) {
  const events = await getEventCalendar({
    start: data.start,
    end: data.end
  });

  const types = await getEventTypes();

  const months = [
    'Jan', 'Feb', 'Mar', 'Apr', 
    'Mei', 'Jun', 'Jul', 'Agu', 
    'Sep', 'Okt', 'Nov', 'Des'
  ]

  events.map(event => {
    const arr = event.start_date.split('-');
    event.date = arr[2];
    event.month = months[parseInt(arr[1])];

    types.data.map(type => {
      if (type.id === event.id_type) {
        event.type = type.type;
      }
      return null;
    })
    return null;
  });

  return events;
}

export async function getEventsAction(data) {
  const events = await getEvents(data);

  const types = await getEventTypes();

  events.data.map(event => {
    types.data.map(type => {
      if (type.id === event.id_type) {
        event.type = type.type;
      }
      return null;
    })
    return null;
  });

  return events;
}

export async function getEventAction(id) {
  return getEvent(id);
}

export async function createEventAction(data) {
  assertNotNullOrBlank(data, 'name', 'Nama Event');
  assertNotNullOrBlank(data, 'point', 'Point');
  assertNotNullOrBlank(data, 'start_date', 'Start Date');
  assertNotNullOrBlank(data, 'id_type', 'Event Type');

  if (data.end_date.length === 0) {
    data.end_date = data.start_date;
  }

  return await createEvent(data);
}

export async function editEventAction(id, data) {
  await editEvent(id, data);
}

export async function deleteEventAction(id) {
  await deleteEvent(id);
}

export function getTypesOfEvents(events) {
  let types = [];

  events.map(event => {
    if (!(types.includes(event.type))) types.push(event.type);
    return null;
  });

  return types;
}

export function getEventsByType(type, events) {
  let result = [];
  events.map(event => {
    if (event.type === type) result.push(event);
    return null;
  });

  return result;
}

export async function uploadFormAction(data) {
  assertNotNullOrBlank(data, 'id_user', 'Id User');
  assertNotNullOrBlank(data, 'id_event', 'Pilihan Event');
  assertNotNullOrBlank(data, 'image', 'Bukti Partisipan');
  assertNotNullOrBlank(data, 'date', 'Tanggal');

  let formData = new FormData();
  formData.append('id_user', data.id_user);
  formData.append('id_event', data.id_event);
  formData.append('date', data.date);
  formData.append('image', data.image);
  formData.append('username', data.username);

  return await createForm(formData);
}

export async function getFormsAction(id_user) {
  const forms = await getForms({id_user: id_user});

  forms.map(form => {
    if (form.status === 'pending') {
      form.status_detail = 'Dalam review'
    } else if (form.status === 'valid') {
      form.status_detail = "Berhasil diterima"
    } else {
      form.status_detail = "Tidak valid"
    }
    return null;
  })

  const order = ['pending', 'valid', 'invalid'];
  return forms.sort((a, b) => order.indexOf(a.status) - order.indexOf(b.status));
}

export async function getPendingFormsAction(data) {
  return await getPendingForms(data);
}

export async function editUserFormsAction(data) {
  await editUserForms(data);
}

export async function deleteUserFormAction(id) {
  await deleteUserForm(id);
}

export async function getEventTypesAction(data) {
  return await getEventTypes(data);
}

export async function getEventTypeAction(id) {
  return getEventType(id);
}

export async function createEventTypeAction(data) {
  assertNotNullOrBlank(data, 'type');
  return await createEventType(data);
}

export async function editEventTypeAction(id, data) {
  assertNotNullOrBlank(data, 'type');
  await editEventType(id, data);
}

export async function deleteEventTypeAction(id) {
  await deleteEventType(id);
}

export async function deleteAllUserFormAction() {
  await deleteAllUserForm();
}