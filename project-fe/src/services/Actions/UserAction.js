import { assertNotNullOrBlank, isPhoneValid } from '../Utils';
import {createUser, auth, createWaGroup, deleteUser, deleteWaGroup, editUser, editWaGroup, exportUsers, getUser, getUsers, getWaGroup, getWaGroups, login, register, resetPassword } from '../Api/User';

export async function registerAction(data) {
  assertNotNullOrBlank(data, 'email', 'Email');
  assertNotNullOrBlank(data, 'password', 'Kata Sandi');
  assertNotNullOrBlank(data, 'password_conf', 'Ulangi Kata Sandi');

  if (!isPhoneValid(data.phone)) {
    throw Error("Nomor Telepon harus berisi angka");
  }

  if (data.password !== data.password_conf) {
    throw Error("Kata sandi tidak sama");
  }

  delete data.password_conf;
  await register(data);
  const response = await login({
    phone: data.phone,
    password: data.password
  });

  localStorage.setItem("token", response.token);
}

export async function loginAction(data) {
  assertNotNullOrBlank(data, 'email', 'Email');
  assertNotNullOrBlank(data, 'password', 'Kata Sandi');

  // if (!isPhoneValid(data.phone)) {
  //   throw Error("Nomor Telepon harus berisi angka");
  // }

  const response = await login({
    email: data.email,
    password: data.password
  });
  // console.log('res',response)
  localStorage.setItem("token", response.data.token);
}

export async function authAction() {
  return await auth();
}

export async function getUserAction(id, nameShorted = false) {
  const user = await getUser(id);

  if (nameShorted) {
    user.name = user.name.split(' ')[0];
  }

  return user;
}

export async function getWaGroupsAction(data = {}) {
  return await getWaGroups(data);
}

export async function getWaGroupAction(id) {
  return getWaGroup(id);
}

export async function createWaGroupAction(data) {
  assertNotNullOrBlank(data, 'name');
  return await createWaGroup(data);
}

export async function editWaGroupAction(id, data) {
  assertNotNullOrBlank(data, 'name');
  await editWaGroup(id, data);
}

export async function deleteWaGroupAction(id) {
  await deleteWaGroup(id);
}


export async function createUserAction(data) {
  
  await createUser({
    id: data.id,
    email: data.email,
    name: data.name,
    phone: data.phone,
    image: data.image,
    address: data.address
  });
}
export async function editProfileAction(data) {
  
  await editUser({
    id: data.id,
    email: data.email,
    name: data.name,
    phone: data.phone,
    image: data.image,
    address: data.address
  });
}

export async function editPasswordAction(id, data) {
  assertNotNullOrBlank(data, 'old_password', "Kata sandi lama");
  assertNotNullOrBlank(data, 'password', "Kata sandi baru");
  assertNotNullOrBlank(data, 'password_conf', "Ulangi kata sandi baru");

  if (data.password !== data.password_conf) {
    throw Error("Kata sandi tidak sama");
  }

  let formData = new FormData();
  formData.append('old_password', data.old_password);
  formData.append('password', data.password);

  await editUser(id, formData);
}

export async function getUsersAction(data) {
  return await getUsers({
    searchKey: data.searchKey,
    page: data.page,
    pageSize: data.pageSize
  })
}

export async function deleteUserAction(id) {
  await deleteUser(id);
}

export async function resetPasswordAction(id) {
  return await resetPassword(id);
}

export async function exportUsersAction() {
  return await exportUsers();
}
