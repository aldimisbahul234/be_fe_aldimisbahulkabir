import { createBanner, deleteBanner, editBanner, getBanner, getBannersData, getHomeBanners, getLandingBanners } from "../Api/Banner";
import { assertNotNullOrBlank } from "../Utils";

export async function getBanners(location) {
  switch (location) {
    case 'home':
      return await getHomeBanners();
    case 'landing':
      return await getLandingBanners();
    default:
      throw Error("Location not found");
  }
}

export async function getBannersAction(data) {
  return await getBannersData(data);
}

export async function getBannerAction(id) {
  return getBanner(id);
}

export async function createBannerAction(data) {
  assertNotNullOrBlank(data, 'location');
  assertNotNullOrBlank(data, 'image');
  assertNotNullOrBlank(data, 'link');

  if (!data.priority) {
    data.priority = 100;
  }

  let formData = new FormData();
  formData.append('location', data.location);
  formData.append('image', data.image);
  formData.append('link', data.link);
  formData.append('priority', data.priority);

  return await createBanner(formData);
}

export async function editBannerAction(id, data) {
  let formData = new FormData();
  formData.append('location', data.location);
  formData.append('link', data.link);
  formData.append('priority', data.priority);

  if (data.image) {
    formData.append('image', data.image);
  }

  await editBanner(id, formData);
}

export async function deleteBannerAction(id) {
  await deleteBanner(id);
}