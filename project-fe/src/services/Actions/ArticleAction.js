import { createArticle, deleteArticle, editArticle, getArticle, getArticlesData } from "../Api/Article";
import { assertNotNullOrBlank } from "../Utils";

export async function getArticlesAction(data) {
  return await getArticlesData(data);
}

export async function getArticleAction(id) {
  return getArticle(id);
}

export async function createArticleAction(data) {
  assertNotNullOrBlank(data, 'title');
  assertNotNullOrBlank(data, 'image');
  assertNotNullOrBlank(data, 'link');

  let formData = new FormData();
  formData.append('title', data.title);
  formData.append('image', data.image);
  formData.append('link', data.link);

  return await createArticle(formData);
}

export async function editArticleAction(id, data) {
  let formData = new FormData();
  formData.append('title', data.title);
  formData.append('link', data.link);
  
  if (data.image) {
    formData.append('image', data.image);
  }

  await editArticle(id, formData);
}

export async function deleteArticleAction(id) {
  await deleteArticle(id);
}