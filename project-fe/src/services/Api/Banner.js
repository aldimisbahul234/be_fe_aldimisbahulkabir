import { BASE_URL, BASE_URL_SECCOND } from '../../config';

const axios = require('axios');

export async function getLandingBanners() {
  const query = {
    location: "landing",
    sortBy: "id",
    order: "desc",
    pageSize: 1000
  };
  
  const response = await axios.get(`${BASE_URL}/banner`, { params: query });
  return response.data.data;
}

export async function getHomeBanners() {
  const query = {
    location: "home",
    sortBy: "id",
    order: "desc",
    pageSize: 1000
  };
  
  const response = await axios.get(`${BASE_URL}/banner`, { params: query });
  return response.data.data;
}

export async function getBannersData(data) {
  const query = {
    location: data.location,
    sortBy: data.sortBy,
    order: data.order,
    page: data.page,
    pageSize: data.pageSize
  };
  
  const response = await axios.get(`${BASE_URL}/banner`, { params: query });
  return response.data;
}

export async function getBanner(id) {
  const response = await axios.get(`${BASE_URL}/banner/${id}`);
  return response.data.data;
}

export async function createBanner(data) {
  const response = await axios.post(`${BASE_URL_SECCOND}/add_banners`, data);
  return response.data.data;
}

export async function editBanner(id, data) {
  await axios.post(`${BASE_URL_SECCOND}/update_banner/${id}`, data);
}

export async function deleteBanner(id) {
  await axios.delete(`${BASE_URL}/banner/${id}`);
}