import { BASE_URL } from '../../config';

const axios = require('axios');

export async function getArticles() {
  try{
    const query = {
      sortBy: "id",
      order: "desc",
      pageSize: 1000
    };
    
    const response = await axios.get(`${BASE_URL}/article`, { params: query });
    return response.data.data;
  }catch(error) {
    return [];
  }
}

export async function getArticlesData(data) {
  const query = {
    searchKey: data.searchKey,
    sortBy: data.sortBy,
    order: data.order,
    page: data.page,
    pageSize: data.pageSize
  };
  
  const response = await axios.get(`${BASE_URL}/article`, { params: query });
  return response.data;
}

export async function getArticle(id) {
  const response = await axios.get(`${BASE_URL}/article/${id}`);
  return response.data.data;
}

export async function createArticle(data) {
  const response = await axios.post(`${BASE_URL}/article`, data);
  return response.data.data;
}

export async function editArticle(id, data) {
  await axios.put(`${BASE_URL}/article/${id}`, data);
}

export async function deleteArticle(id) {
  await axios.delete(`${BASE_URL}/article/${id}`);
}