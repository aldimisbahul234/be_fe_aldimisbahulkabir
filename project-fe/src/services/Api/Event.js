import { BASE_URL, BASE_URL_SECCOND } from '../../config';

const axios = require('axios');

export async function getEventCalendar(data) {
  const query = {
    sortBy: "start_date",
    order: "asc",
    pageSize: 1000,
    one_day_event: 1,
    start_date: data.start,
    end_date: data.end
  };
  
  const response = await axios.get(`${BASE_URL}/event`, { params: query });
  return response.data.data;
}

export async function getEvents(data) {
  const query = {
    searchKey: data.searchKey,
    sortBy: data.sortBy || "start_date",
    order: data.order || "desc",
    page: data.page,
    pageSize: data.pageSize || 1000,
    date: data.date
  };
  
  const response = await axios.get(`${BASE_URL}/event`, { params: query });
  return response.data;
}

export async function getEvent(id) {
  const response = await axios.get(`${BASE_URL}/event/${id}`);
  return response.data.data;
}

export async function createEvent(data) {
  const response = await axios.post(`${BASE_URL}/event`, data);
  return response.data.data;
}

export async function editEvent(id, data) {
  await axios.put(`${BASE_URL}/event/${id}`, data);
}

export async function deleteEvent(id) {
  await axios.delete(`${BASE_URL}/event/${id}`);
}

export async function getEventTypes(data = {}) {
  const query = {
    searchKey: data.searchKey,
    sortBy: "id",
    order: "desc",
    page: data.page,
    pageSize: data.pageSize || 1000
  };
  const response = await axios.get(`${BASE_URL}/event/type`, { params: query });
  return response.data;
}

export async function getEventType(id) {
  const response = await axios.get(`${BASE_URL}/event/type/${id}`);
  return response.data.data;
}

export async function createEventType(data) {
  const response = await axios.post(`${BASE_URL}/event/type`, data);
  return response.data.data;
}

export async function editEventType(id, data) {
  await axios.put(`${BASE_URL}/event/type/${id}`, data);
}

export async function deleteEventType(id) {
  await axios.delete(`${BASE_URL}/event/type/${id}`);
}

export async function createForm(formData) {
  return await axios.post(`${BASE_URL_SECCOND}/submit_form`, formData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  });
}

export async function getForms(data) {
  const query = {
    id_user: data.id_user,
    sortBy: "date",
    order: "desc",
    pageSize: 1000
  };
  const response = await axios.get(`${BASE_URL}/event/form`, { params: query });
  return response.data.data;
}

export async function getPendingForms(data) {
  const query = {
    id_event: data.id_event,
    sortBy: data.sortBy ? data.sortBy : "id",
    order: data.order ? data.order : "desc",
    status: "pending",
    page: data.page,
    pageSize: data.pageSize,
    searchKey: data.searchKey
  };
  const response = await axios.get(`${BASE_URL_SECCOND}/list_event_form_pending`, { params: query });
  return response.data;
}

export async function editUserForms(data) {
  await axios.put(`${BASE_URL}/event/form`, data);
}

export async function deleteUserForm(id) {
  await axios.delete(`${BASE_URL}/event/form/user/${id}`);
}

export async function deleteAllUserForm() {
  await axios.delete(`${BASE_URL}/event/form`);
}