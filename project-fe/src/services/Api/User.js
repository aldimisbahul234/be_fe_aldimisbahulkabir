import { BASE_URL } from '../../config';

const axios = require('axios');

export async function auth() {
  const response = await axios.get(`${BASE_URL}/users`);
  return response.data.data;
}

export async function login(data) {
  const response = await axios.post(`${BASE_URL}/login`, data);
  return response;
}

export async function register(data) {
  const response = await axios.post(`${BASE_URL}/user`, data);
  return response.data.data;
}

export async function getUser(id) {
  const response = await axios.get(`${BASE_URL}/users/detail/${id}`);
  return response.data.users;
}


export async function createUser(createUser) {
  await axios.post(`${BASE_URL}/users/create`, createUser, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  });
}
export async function editUser(formData) {
  await axios.post(`${BASE_URL}/users/update`, formData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  });
}

export async function getWaGroups(data) {
  const query = {
    searchKey: data.searchKey,
    sortBy: "id",
    order: "desc",
    page: data.page,
    pageSize: data.pageSize
  };
  const response = await axios.get(`${BASE_URL}/wa-group`, { params: query });
  return response.data;
}

export async function getWaGroup(id) {
  const response = await axios.get(`${BASE_URL}/wa-group/${id}`);
  return response.data.data;
}

export async function createWaGroup(data) {
  const response = await axios.post(`${BASE_URL}/wa-group`, data);
  return response.data.data;
}

export async function editWaGroup(id, data) {
  await axios.put(`${BASE_URL}/wa-group/${id}`, data);
}

export async function deleteWaGroup(id) {
  await axios.delete(`${BASE_URL}/wa-group/${id}`);
}

export async function getUsers(data) {
  const query = {
    searchKey: data.searchKey,
    page: data.page,
    pageSize: data.pageSize,
    sortBy: 'id',
    order: 'asc'
  };
  const response = await axios.get(`${BASE_URL}/users`, { params: query });
  console.log(response)
  return response.data;
}

export async function deleteUser(id) {
  await axios.post(`${BASE_URL}/users/delete/${id}`);
}

export async function resetPassword(id) {
  const response = await axios.post(`${BASE_URL}/user/password/reset`, {id_user: id});
  return response.data.data;
}

export async function exportUsers() {
  return await axios({
    url: `${BASE_URL}/user/export/xlsx`,
    method: 'GET',
    responseType: 'blob'
  });
}