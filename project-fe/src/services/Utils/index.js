export function assertNotNullOrBlank(data, key, name) {
  if (!(key in data)) {
    throw Error(`${name} tidak boleh kosong`);
  }

  if (!data[key] || data[key].length === 0) {
    throw Error(`${name} tidak boleh kosong`);
  }
}

export function isPhoneValid(phone) {
  if (phone.length >= 8 && phone.length <= 13) {
    return /^\d+$/.test(phone);
  }

  return false;
}

export function errorHandler(e, setMessage) {
  if (e.response && e.response.data) {
    const msg = e.response.data.message;
    setMessage(msg);
  } else {
    setMessage(e.message);
  }
}

export function formatDate(t) {
  const date = ('0' + t.getDate()).slice(-2);
  const month = ('0' + (t.getMonth() + 1)).slice(-2);
  const year = t.getFullYear();
  return `${year}-${month}-${date}`;
}