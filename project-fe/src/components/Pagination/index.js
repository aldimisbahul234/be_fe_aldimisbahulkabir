import './index.css';

const Pagination = (props) => {
  return (
    <div className="parent-pagination">
      <button onClick={props.prev}>Prev</button>
      <div>{props.current}/{props.total}</div>
      <button onClick={props.next}>Next</button>
    </div>
  )
}

export default Pagination;