import { useEffect, useState } from 'react';
import Arrow from '../../assets/arrow_purple.png';

import { getArticles } from '../../services/Api/Article';
import { IMAGES_URL } from '../../config';

import './index.css';

const Article = () => {
  const [articles, setArticles] = useState([]);

  useEffect(() => {
    getArticles()
      .then(res => { setArticles(res); })
  }, []);

  return (
    <div className='articles'>
      {
        articles.map(article => (    
          <a href={ article.link } key={article.id} target='_blank' rel='noreferrer'>
            <div className='item'>
              <img src={`${IMAGES_URL}/${article.image}`} alt="Artikel"/>
              <div className='content'>
                <div className='see-full'>
                  <div>Selengkapnya</div>
                  <img src={ Arrow } alt="Arrow"/>
                </div>
                <div className='article-title'>
                  <div>{article.title}</div>
                </div>
              </div>
            </div>
          </a>
        ))
      }
    </div>
  )
}

export default Article;