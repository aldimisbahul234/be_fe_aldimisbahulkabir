import { Link, useLocation } from 'react-router-dom';
import { useState, useEffect } from "react";
import { FaBeer, FaUserAlt, FaRocketchat, FaClipboardList, FaHotjar, FaPhotoVideo, FaPowerOff, FaUserEdit, FaUserCog, FaTrash } from 'react-icons/fa';
import { BsArrowDownUp } from "react-icons/bs";
import './index.css';

const AdminMenu = () => {
  const location = useLocation();
  const [tabActiveUser, settabActiveUser] = useState(null);
  const [tabActiveBanner, settabActiveBanner] = useState(null);
  console.log(localStorage)
  const logout = () => {
    localStorage.clear();
    window.location.reload();
  }

  useEffect(() => {
    if(location.pathname ==="/user"){
      settabActiveUser('active')
      settabActiveBanner(null)
    }
  })
  return (
    <div className="section-menu" style={{paddingTop: '40px'}}>
      <div className='icon-container' style={{marginBottom: '50px', marginLeft: '10px'}}>
        <img src="https://cdn2.iconfinder.com/data/icons/flatfaces-everyday-people-square/128/beard_male_man_face_avatar-512.png" />
        <div className='status-circle'></div>
        <label>Hallo admin</label>
      </div>
        <div className="vertical-tab" role="tabpanel">
                <ul className="nav nav-tabs" role="tablist">
                    <li style={{marginBottom: '19px'}} className={tabActiveUser}><Link style={{textDecoration: 'none'}} to='/user'><FaUserAlt /> Manage User</Link></li>
                   
                </ul>
          </div>
      <button onClick={logout}><FaPowerOff /> Keluar</button>
    </div>
  )
}

export default AdminMenu;