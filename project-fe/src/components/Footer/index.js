import { Link } from 'react-router-dom';

import './index.css';

import FooterBackground from '../../assets/footer_bg.png';
import Logo from '../../assets/logo.png';
import Facebook from '../../assets/fb_logo.png';
import Instagram from '../../assets/ig_logo.png';
import Youtube from '../../assets/youtube_logo.png';

const links = {
  email: "https://lynk.id/link/redirect/to?n=email&link=mailto:community@bertsolution.com&u=@ibu2canggih",
  whatsapp: "https://wa.me/6281326035476?text=Halo%2C%20saya%20ingin%20bekerjasama%20dengan%20Ibu2Canggih",
  facebook: "https://www.facebook.com/groups/1065991477161916/?ref=share",
  instagram: "https://instagram.com/ibu2canggih",
  youtube: "https://youtube.com/c/IbuIbuCanggih"
};

const Footer = () => {
  return (
    <div className="footer overlay-container">
      <img className='background' src={ FooterBackground } alt='Footer Background'/>
      <div className='content'>
        <p>FOR BUSINESS INQUIRIES</p>

        <div className='contact'>
          <Link to='/' className='box-logo'><img className='logo' src={ Logo } alt="Logo"/></Link>
          <div className='contact-info'>
            <a href={ links['email'] } target='_blank' rel="noreferrer">
              <b>Email</b><br/>Ibu2canggih@gmail.com
            </a>
          </div>
          <div className='divider' />
          <div className='contact-info'>
            <a href={ links['whatsapp'] } target='_blank' rel="noreferrer">
              <b>Whatsapp</b><br/>(+62) 813-2603-5476
            </a>
          </div>
        </div>

        <div className='socmed'>
          <a href={ links['facebook'] } target='_blank' rel="noreferrer">
            <img src={ Facebook } alt="Facebook"/>
          </a>
          <a href={ links['instagram'] } target='_blank' rel="noreferrer">
            <img src={ Instagram } alt="Instagram"/>
          </a>
          <a href={ links['youtube'] } target='_blank' rel="noreferrer">
            <img src={ Youtube } alt="Youtube"/>
          </a>
        </div>

        <div className='copyright'>© 2022 Ibu2Canggih</div>
      </div>
    </div>
  )
}

export default Footer;