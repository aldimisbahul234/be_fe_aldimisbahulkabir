import './index.css';

import HomeActive from '../../assets/home_active.png';
import HomeInactive from '../../assets/home_inactive.png';
import ProfileActive from '../../assets/profile_active.png';
import ProfileInactive from '../../assets/profile_inactive.png';
import ArticleActive from '../../assets/article_active.png';
import ArticleInactive from '../../assets/article_inactive.png';
import CalendarActive from '../../assets/calendar_active.png';
import CaledarInactive from '../../assets/calendar_inactive.png';
import { Link } from 'react-router-dom';

const Navbar = (props) => {

  const images = {
    home: HomeInactive,
    profile: ProfileInactive,
    article: ArticleInactive,
    calendar: CaledarInactive
  }

  const styles = {
    home: 'inactive',
    profile: 'inactive',
    article: 'inactive',
    calendar: 'inactive'
  }

  switch (props.active) {
    case 'home':
      images.home = HomeActive;
      styles.home = 'active';
      break;
    case 'profile':
      images.profile = ProfileActive;
      styles.profile = 'active';
      break;
    case 'article':
      images.article = ArticleActive;
      styles.article = 'active';
      break;
    case 'calendar':
      images.calendar = CalendarActive;
      styles.calendar = 'active';
      break;
    default:
      break;
  }

  return (
    <div className='navbar'>
      <Link to="/" className={`item ${styles.home}`}>
        <img src={images.home} alt="Home"/>
        <p>Beranda</p>
      </Link>

      <Link to="/profile" className={`item ${styles.profile}`}>
        <img src={images.profile} alt="Profile"/>
        <p>Profil</p>
      </Link>

      <Link to="/article" className={`item ${styles.article}`}>
        <img src={images.article} alt="Article"/>
        <p>Artikel</p>
      </Link>

      <Link to="/calendar" className={`item ${styles.calendar}`}>
        <img src={images.calendar} alt="Calendar"/>
        <p>Kalender</p>
      </Link>
    </div>
  )
};

export default Navbar;