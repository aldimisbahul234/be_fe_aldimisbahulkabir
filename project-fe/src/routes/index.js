import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { BrowserRouter, Route, Routes, Navigate } from 'react-router-dom';

import UserPage from '../pages/admin/User';
import AdminUserEditPage from '../pages/admin/UserEdit';

import AdminUserCreatePage from '../pages/admin/UserCreate';


import LoginPage from '../pages/general/Login';
import { authAction } from '../services/Actions/UserAction';

/**
 * Routes component containing routes for the whole application
 * @returns {JSX}
 */

export const userIdContext = React.createContext(null);

const RoutesManager = () => {
  const [auth, setAuth] = useState({});

  useEffect(() => {
    const token = localStorage.getItem("token");
    // axios.defaults.headers.common.token = token;
    // axios.defaults.headers.Authorization = `Bearer ${token}`;
    axios.defaults.headers.Authorization = `Bearer ${token}`;
   
    

    if (token) {
      // authAction()
      //   .then(res => {
      //     console.log(res)
          setAuth({role: "admin"});
        // })
        // .catch(e => { setAuth({role: "general"}) });
    } else {
      setAuth({role: "general"});
    }
  }, []);

  return (
    <BrowserRouter>
        {
          auth.role === "general" && (
            <div className="App">
              <Routes>
                {/* <Route path='/' element={< LandingPage />} /> */}
                <Route path='/' element={< LoginPage />} />
               
                <Route path='*' element={ <Navigate to ='/' /> } />
              </Routes>
            </div>
          )
        }

        

{
          auth.role === "admin" && (
            <div>
              <Routes>
                <Route path='/user' element={< UserPage />} />
                <Route path='/user/:id' element={< AdminUserEditPage />} />
                <Route path='/user/create' element={< AdminUserCreatePage />} />
                <Route path='*' element={ <Navigate to ='/user' /> } />
              </Routes>
            </div>
          )
        }
    </BrowserRouter>
  )
};

export default RoutesManager;